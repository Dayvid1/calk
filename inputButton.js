import React, {Component} from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

export default class InputButton extends Component{
    
    render(){
        return(
           <TouchableOpacity style={[styles.input_button, this.props.higlight ? styles.inputhighlightButton: null ]} onPress={this.props.onPress}>
            <Text style={styles.input_button_text}>{this.props.value}</Text>
        </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    input_button:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#ff7f24',
        borderWidth:1
        
    },
    input_button_text:{
        fontWeight:'bold',
        color:'white',
        fontSize:22
    },
    inputhighlightButton:{
        backgroundColor:'white'
    }
})