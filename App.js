import React,{Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import InputButton from './inputButton';

const inputButton = [
  [1, 2, 3, '/'],
  [4, 5, 6, '*'],
  [7, 8, 9, '-'],
  [0, '.', '=', '+']
];

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      inputValue : 0,
      previousInputValue: 0,
      selectedSymbol: null
    }
  }
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.resultScreen}>
          <Text style={styles.displayText}>{this.state.inputValue}</Text>
        </View>
        <View style={styles.inputScreen}>
          {this._inputrenderButton()}
        </View>
      </View>);
  }
  
  InputButtonPressed=(input)=>{
    switch(typeof input){
      case 'number':
        return this.handleNumberPressed(input)
      case 'string':
        return this.handleStringPressed(input)
    }
  }
  
handleNumberPressed =(num)=>{
  let inputValue = (this.state.inputValue * 10) + num;

  this.setState({
    inputValue:inputValue
  })
}

handleStringPressed = (str)=>{
  switch(str){
    case '/':
    case '*':
    case '+':
    case '-':
       this.setState({
        selectedSymbol: str,
        previousInputValue: this.state.inputValue,
        inputValue: 0
      });
    case '=':
      let symbol = this.state.selectedSymbol,
      inputValue = this.state.inputValue,
      previousInputValue = this.state.previousInputValue;

      if(!symbol){
        return;
      }
      this.setState({
        previousInputValue:0,
        inputValue: eval(previousInputValue + symbol + inputValue ),
        selectedSymbol:null
      })
      break;
  }

}

_inputrenderButton(){
    let views = [];

    for (var r = 0; r < inputButton.length; r ++) {
        let row = inputButton[r];

        let inputRow = [];
        for (var i = 0; i < row.length; i ++) {
            let input = row[i];

            inputRow.push(
                <InputButton 
                  value={input}
                  highlight={this.state.selectedSymbol === input}
                  onPress={this.InputButtonPressed.bind(this,input)}
                  key={r + "-" + i} />
            );
        }
        views.push(<View style={styles.inputRow} key={"row-" + r}>{inputRow}</View>)
    }
    return views
 }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#cd661d',
  },
  resultScreen:{
    backgroundColor:'#98f5ff',
    flex:2,
    justifyContent:'center'
  },
  inputScreen:{
    backgroundColor:'#53868b',
    flex:8
  },
  inputRow:{
    flex:1,
    flexDirection:'row'
  },
  displayText:{
    color: '#458b74',
    textAlign:'right',
    padding:20,
    fontWeight:'bold',
    fontSize:38
  }
});

export default App;